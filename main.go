package main

import (
	"fmt"
	"log"
	"os"

	socks5 "github.com/markupCode/go-socks5"
	socksserver "github.com/markupCode/socks-server/src"
)

func main() {
	storage := socksserver.EnvironmentStorage{}

	creds := socks5.StaticCredentials{
		"username": storage.Variable("AUTH_NAME"),
		"password": storage.Variable("AUTH_PASSWORD"),
	}

	cator := socks5.UserPassAuthenticator{Credentials: creds}
	conf := &socks5.Config{
		AuthMethods: []socks5.Authenticator{cator},
		Logger:      log.New(os.Stdout, "", log.LstdFlags),
	}

	server, err := socks5.New(conf)
	if err != nil {
		panic(err)
	}

	ip := fmt.Sprintf("127.0.0.1:%s", storage.Variable("PORT"))

	if err := server.ListenAndServe("tcp", ip); err != nil {
		panic(err)
	}
}
