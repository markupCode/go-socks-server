package socksserver

import (
	"fmt"
	"os"
)

// EnvironmentStorage reporesent storage for environment vars
type EnvironmentStorage struct{}

// Variable return environment variable
func (storage *EnvironmentStorage) Variable(key string) string {
	variable := os.Getenv(key)

	if !validVar(variable) {
		panic(fmt.Errorf("variable was not set or empty: %s", key))
	}

	return variable
}

func validVar(variable string) bool {
	return variable != ""
}
